﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogSystem : MonoBehaviour
{
    [Header("Dialog")]
    public DialogData dialog;
    public Text inGameText;

    [Header("Dialog Start")]
    public Text dialogeStartText;
    public bool dialogCanStart;
    public bool dialogStarted;
    public float dialogRadius;
    public GameObject playerObject;

    [Header("UI disables")]
    public GameObject[] uiDisables;

    public int index;

    [Header("Response buttons")]
    public Button[] buttons;
    public Text[] buttonText;

    private static DialogSystem instance;
    private void instantiateSingleton()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            if (instance != this)
            {
                Destroy(this.gameObject);
            }
        }
    }

    private DialogSystem()
    {

    } 

    public void Start()
    {
        fillButtonsText();
        dialogeStartText.enabled = false;

        buttons[0].onClick.AddListener(getDialogResponseFirst);
        buttons[1].onClick.AddListener(getDialogResponseSecond);
        buttons[2].onClick.AddListener(getDialogResponseThird);
        buttons[3].onClick.AddListener(getDialogResponseForth);
        buttons[4].onClick.AddListener(getDialogResponseFifth);
        buttons[5].onClick.AddListener(getDialogResponseSixth);
    }

    private void Update()
    {
        checkResponseOption();
    }

    public bool activateDialog(GameObject gameObject, GameObject player)
    {
        return (Vector3.Distance(gameObject.transform.position, player.transform.position) < dialogRadius);
    }

    public void fillButtonsText()
    {
        index = 0;
        inGameText.text = dialog.dialogStruct[index].dialogText;

        for (int i = 0; i < buttonText.Length; i++)
        {
            buttonText[i].text = dialog.dialogOptions[i].responseText;
        }
    }


    //public void TryStartDialog(DialogData dialogData){
    //    Debug.Log("TryStartDialog " + dialogData.dialogId);

    //    if (dialogCanStart)
    //    {
    //        dialogStarted = !dialogStarted;

    //        foreach (GameObject gameObject in uiDisables)
    //        {
    //            gameObject.SetActive(dialogCanStart);
    //        }
    //        dialogeStartText.enabled = false;
    //    }
    //}

    public void checkResponseOption()
    {
        if (activateDialog(this.gameObject, playerObject))
        {
            if (!dialogCanStart)
            {
                dialogeStartText.enabled = true;
            }
            dialogCanStart = true;

            if (Input.GetKeyDown(KeyCode.F))
            {
                /*
                 * Quest: Dialog Started
                 
                 */
                /*
                QuestManager questManager = QuestManager.GetInstance();
                questManager.DialogStarted(getDialogId());
                */

                dialogStarted = true;

                foreach (GameObject gameObject in uiDisables)
                {
                    gameObject.SetActive(dialogCanStart);
                }
                dialogeStartText.enabled = false;
            }
        }
        else
        {
            dialogCanStart = false;
            dialogeStartText.enabled = dialogCanStart;

            foreach (GameObject gameObject in uiDisables)
            {
                gameObject.SetActive(dialogCanStart);
            }
        }
    }

    public void getDialogResponseFirst()
    {
        if (dialog.dialogStruct[index].dialogResponseWithOption[0] <= dialog.dialogStruct.Length)
        {
            index = dialog.dialogStruct[index].dialogResponseWithOption[0];
            inGameText.text = dialog.dialogStruct[index].dialogText;
        }
        else
        {
            dialogStarted = false;
            endDialog();
        }
    }

    public void getDialogResponseSecond()
    {
        if (dialog.dialogStruct[index].dialogResponseWithOption[1] <= dialog.dialogStruct.Length)
        {
            index = dialog.dialogStruct[index].dialogResponseWithOption[1];
            inGameText.text = dialog.dialogStruct[index].dialogText;
        }
        else
        {
            dialogStarted = false;
            endDialog();
        }
    }

    public void getDialogResponseThird()
    {
        if (dialog.dialogStruct[index].dialogResponseWithOption[2] <= dialog.dialogStruct.Length)
        {
            index = dialog.dialogStruct[index].dialogResponseWithOption[2];
            inGameText.text = dialog.dialogStruct[index].dialogText;
        }
        else
        {
            dialogStarted = false;
            endDialog();
        }
    }

    public void getDialogResponseForth()
    {
        if (dialog.dialogStruct[index].dialogResponseWithOption[3] <= dialog.dialogStruct.Length)
        {
            index = dialog.dialogStruct[index].dialogResponseWithOption[3];
            inGameText.text = dialog.dialogStruct[index].dialogText;
        }
        else
        {
            dialogStarted = false;
            endDialog();
        }
    }

    public void getDialogResponseFifth()
    {
        if (dialog.dialogStruct[index].dialogResponseWithOption[4] <= dialog.dialogStruct.Length)
        {
            index = dialog.dialogStruct[index].dialogResponseWithOption[4];
            inGameText.text = dialog.dialogStruct[index].dialogText;
        }
        else
        {
            dialogStarted = false;
            endDialog();
        }
    }

    public void getDialogResponseSixth()
    {
        if (dialog.dialogStruct[index].dialogResponseWithOption[5] <= dialog.dialogStruct.Length)
        {
            index = dialog.dialogStruct[index].dialogResponseWithOption[5];
            inGameText.text = dialog.dialogStruct[index].dialogText;
        }
        else
        {
            dialogStarted = false;
            endDialog();
        }
    }


    public void endDialog()
    {
            dialogeStartText.enabled = true;

            foreach (GameObject gameObject in uiDisables)
            {
                gameObject.SetActive(false);
            }
    }

    public bool getDialogStart()
    {
        return dialogStarted;
    }

    public string getDialogId()
    {
        return dialog.dialogId;
    }
}
