﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(menuName = "DatingSim/Create new DialogData")]
public class DialogData : ScriptableObject
{
    public string dialogId;

    [Header("Dialouge")]
    public DialogeStruct[] dialogStruct;
    

    [Header("Response Option")]
    public DialogResponseStruct[] dialogOptions = new DialogResponseStruct[6];

    [Header("CharaterData")]
    public CharacterData characterData;

    [Serializable]
    public struct DialogeStruct
    {
        // The Dialogtext that gets displayed
        [TextArea(1,20)]
        public string dialogText;

        // Get FameStats
        public float[] stats;

        [Header("Next Dialoges with Buttons")]
        public int[] dialogResponseWithOption;
    }

    [Serializable]
    public struct DialogResponseStruct
    {
        public string responseText;
    }
}
