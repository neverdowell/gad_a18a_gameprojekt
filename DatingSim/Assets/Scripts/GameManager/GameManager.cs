﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class GameManager : MonoBehaviour {

    [Header("Managers")]
    public DialogSystem dialogSystem;
    public QuestManager questManager;
    public PlayerState playerState;
    public MessageManager messageManager;

    [Header("Balancing Data")]
    public ItemData[] itemDatas;
    public CharacterData[] characterDatas;
    public QuestData[] questDatas;




    private string gameStatePath;
    private string playerStatePath;




    #region singleton
    private static GameManager instance;
    private GameManager() { }

    private void Awake()
    {
        instantiateSingleton();
    }

    private void instantiateSingleton() {
        if (instance == null) {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
            gameStatePath = Application.persistentDataPath + "gamesate.dat";
            playerStatePath = Application.persistentDataPath + "playerstate.dat";
        } else {
            if (instance != this) {
                Destroy(this.gameObject);
            }
        }
    }

    public static GameManager GetInstance() {
        return instance;
    }
    #endregion


    public void SaveGameState()
    {
        // Set GameState
        GameState gameState = new GameState();
        gameState.activeQuests = questManager.GetActiveQuests();
        gameState.finishedQuests = questManager.GetFinishedQuests();

        // Serialize GameState and save to file
        FileStream fileStream;
        if (File.Exists(gameStatePath))
        {
            fileStream = File.OpenWrite(gameStatePath);
        } else
        {
            fileStream = File.Create(gameStatePath);
        }

        BinaryFormatter binaryFormatter = new BinaryFormatter();
        binaryFormatter.Serialize(fileStream, gameState);
        fileStream.Close();
    }

    public void LoadGameState()
    {
        // Deserialize GameState from file
        FileStream fileStream;
        if (File.Exists(gameStatePath))
        {
            fileStream = File.OpenRead(gameStatePath);
        } else
        {
            Debug.LogWarning($"{gameStatePath} konnte nicht gefunden werden.");
            return;
        }

        BinaryFormatter binaryFormatter = new BinaryFormatter();
        GameState gameState = (GameState)binaryFormatter.Deserialize(fileStream);

        fileStream.Close();

        // Read GameState
        questManager.LoadActiveQuests(gameState.activeQuests);
        questManager.LoadFinishedQuests(gameState.finishedQuests);

    }

    public void SavePlayerState()
    {
        FileStream fileStream;
        if (File.Exists(playerStatePath))
        {
            fileStream = File.OpenWrite(playerStatePath);
        } else
        {
            fileStream = File.Create(playerStatePath);
        }

        BinaryFormatter binaryFormatter = new BinaryFormatter();
        binaryFormatter.Serialize(fileStream, playerState);
        fileStream.Close();
    }

    public void LoadPlayerState()
    {
        FileStream fileStream;
        if (File.Exists(playerStatePath))
        {
            fileStream = File.OpenRead(playerStatePath);
        } else
        {
            Debug.LogWarning($"{playerStatePath} konnte nicht gefunden werden.");
            return;
        }

        BinaryFormatter binaryFormater = new BinaryFormatter();
        PlayerState playerState = (PlayerState)binaryFormater.Deserialize(fileStream);

        fileStream.Close();

        this.playerState = playerState;
    }

    // Start is called before the first frame update
    void Start()
    {
        LoadGameState();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    #region helper
    public ItemData GetItemData(int itemDataId) {
        foreach (ItemData itemData in itemDatas) {
            if (itemData.itemId == itemDataId) {
                return itemData;
            }
        }
        return null;
    }

    public Sprite GetItemImage( int itemDataId ) {
        ItemData itemData = GetItemData(itemDataId);
        if (itemData != null) {
            return itemData.sprite;
        }
        return null;
    }

    public CharacterData GetCharacterData(int characterDataId) {
        foreach (CharacterData characterData in characterDatas) {
            if (characterData.characterId == characterDataId) {
                return characterData;
            }
        }
        return null;
    }

    public Sprite GetCharacterImage(int characterDataId ) {
        CharacterData characterData = GetCharacterData(characterDataId);
        if (characterData != null) {
            return characterData.sprite;
        }
        return null;
    }

    //public QuestData GetQuestData( int itemDataId ) {
    //    foreach (ItemData itemData in itemDatas) {
    //        if (itemData.itemId == itemDataId) {
    //            return itemData;
    //        }
    //    }
    //    return null;
    //}
    #endregion
}
