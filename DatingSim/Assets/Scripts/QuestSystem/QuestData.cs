﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "DatingSim/Create new QuestData")]
public class QuestData : ScriptableObject
{
    public new string name;
    public string questID;
    public bool isActive;
    public bool isFinished;

    public string title;
    public string desc;

    /// <summary>
    /// Int-Array mit den Werten der Rewards. Um ein Reward zu deaktivieren, Wert auf 0 setzen. 
    /// Array speichert daten im folgenden Schema: int[Charisma, SexAppeal, Intelligenz, Stärke, Gold]
    /// </summary>
    public int[] rewards = new int[5];

    public QuestGoal goal;

}
