﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestButton : MonoBehaviour
{
    private QuestData quest;

    public QuestData Quest { get => quest; set => quest = value; }

    public void ChangeSelectedQuest()
    {
        QuestManager.GetInstance().SelectQuest(quest);
    }
}
