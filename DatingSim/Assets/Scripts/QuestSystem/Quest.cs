﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Quest
{
    public string questID;

    public bool isActive;
    public bool isFinished;
    public bool isSubQuest;

    public string title;
    public string desc;

    /// <summary>
    /// Int-Array mit den Werten der Rewards. Um ein Reward zu deaktivieren, Wert auf 0 setzen. 
    /// Array speichert daten im folgenden Schema: int[Charisma, SexAppeal, Intelligenz, Stärke, Gold]
    /// </summary>
    public int[] rewards;

    public QuestGoal goal;
}
