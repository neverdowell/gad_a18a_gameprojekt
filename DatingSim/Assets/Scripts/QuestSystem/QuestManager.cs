﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestManager : MonoBehaviour
{
    // public variables here
    // ...
    [Header("Player stuff")]
    public QuestPlayer player = null;
    public Text healthText;
    public Text playerMoney;

    [Header("QuestLog UI")]
    public GameObject questLog;
    public GameObject questInfo;

    public GameObject questButtonParent;
    public GameObject questButtonPrefab;

    [Header("QuestInfo UI")]
    public Text questTitle;
    public Text questDesc;
    public Text questRewardCharisma;
    public Text questRewardSexAppeal;
    public Text questRewardGold;
    public Text questRewardIntelligence;
    public Text questRewardStrength;


    //public Quest[] questsArray;
    public QuestData[] questsArray;

    // private variables here
    // ...
    //private PlayerState ps = GameManager.GetInstance().playerState;
    private PlayerState ps = new PlayerState();

    private QuestData selectedQuest;

    #region singleton
    private static QuestManager instance;
    private QuestManager() { }

    private void instantiateSingleton()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            if (instance != this)
            {
                Destroy(this.gameObject);
            }
        }
    }

    public static QuestManager GetInstance()
    {
        return instance;
    }
    #endregion

    #region lifecycle
    private void Awake()
    {
        instantiateSingleton(); // THIS HAS TO BE FIRST CALL IN AWAKE!

        GetAllQuestButtons();
    }

    private void Update()
    {
        // DO NOT CALL IN UPDATE, BAD PERFORMANCE; Instead call in associated functions of other systems
        // CheckQuestCompleted();
    }

    private void Start()
    {
        Debug.Log(" assdf " + (GameManager.GetInstance() == null));
        Debug.Log(" assdf " + (GameManager.GetInstance().messageManager == null));
        MessageManager mM = GameManager.GetInstance().messageManager;

        mM.AddMessage("Quest beendet.");
    }
    #endregion

    #region logic

    #region QuestLog stuff
    public void ShowQuestLog()
    {
        questLog.SetActive(!questLog.activeSelf);

        ShowActiveQuests();
    }

    /// <summary>
    /// Shows only the active Quests in the Questlog
    /// </summary>
    public void ShowActiveQuests()
    {
        if (selectedQuest == null)
        {
            questInfo.SetActive(false);
        }

        Button[] questButtons = questLog.GetComponentsInChildren<Button>(true);

        foreach (Button b in questButtons)
        {
            QuestData q = b.GetComponent<QuestButton>().Quest;

            b.GetComponentInChildren<Text>().text = q.title;

            if (q.isActive)
            {
                b.gameObject.SetActive(true);
            }
            else
            {
                b.gameObject.SetActive(false);
            }
        }
    }

    public void SelectQuest(QuestData select)
    {
        questInfo.SetActive(true);

        selectedQuest = select;

        Button[] questButtons = questLog.GetComponentsInChildren<Button>();

        foreach (var b in questButtons)
        {
            if (b.GetComponent<QuestButton>().Quest != selectedQuest)
            {
                b.interactable = true;
            }
        }

        RefreshQuestInfo();
    }

    public void RefreshQuestInfo()
    {
        questTitle.text = selectedQuest.title;
        questDesc.text = selectedQuest.desc;

        questRewardCharisma.text = "Charisma: " + selectedQuest.rewards[0].ToString();
        questRewardSexAppeal.text = "Sexuelle Ausstrahlung: " + selectedQuest.rewards[1].ToString();
        questRewardIntelligence.text = "Intelligenz: " + selectedQuest.rewards[2].ToString();
        questRewardStrength.text = "Stärke: " + selectedQuest.rewards[3].ToString();
        questRewardGold.text = "Gold: " + selectedQuest.rewards[4].ToString();

        Text[] a = { questRewardCharisma, questRewardSexAppeal, questRewardIntelligence, questRewardStrength, questRewardGold };

        for (int i = 0; i < selectedQuest.rewards.Length; i++)
        {
            if (selectedQuest.rewards[i] == 0)
            {
                a[i].gameObject.SetActive(false);
            }
            else
            {
                a[i].gameObject.SetActive(true);
            }
        }
    }

    public void GetAllQuestButtons()
    {
        foreach (var q in questsArray)
        {
            GameObject go = Instantiate(questButtonPrefab);
            go.transform.SetParent(questButtonParent.transform);

            go.GetComponent<QuestButton>().Quest = q;
            go.name = "QuestData" + q.questID;
        }
    }

    #endregion

    public void StartQuest(string id)
    {
        foreach (QuestData q in questsArray)
        {
            if (q.questID == id)
            {
                q.isActive = true;
            }
        }
    }

    /// <summary>
    /// Überprüft alle Quests, ob diese schon fertig sind. Sind die Quests noch nicht fertig, werden die Quest und das Objective and die Logik übergeben.
    /// 
    /// Objective ist hierbei das Objective der Quest. Z. B. das aufgesammelte Item oder die Person mit der man redet.
    /// </summary>
    /// <param name="objective"></param>
    public void CheckQuestCompleted()
    {
        foreach (var quest in questsArray)
        {
            if (quest.isActive)
            {
                if (quest.isFinished)
                {
                    MessageManager mM = GameManager.GetInstance().messageManager;

                    mM.AddMessage($"Quest '{quest.title}' beendet.");

                    //Give player rewards
                    for (int i = 0; i < quest.rewards.Length; i++)
                    {
                        if (quest.goal.type != QuestGoal.GoalType.Gather)
                        {
                            ps.charisma += quest.rewards[0];
                            ps.sexappeal += quest.rewards[1];
                            ps.intelligence += quest.rewards[2];
                            ps.strength += quest.rewards[3];
                            ps.gold += quest.rewards[4];
                        }
                    }


                    //Remove completed Quest
                    quest.isActive = false;
                }
                else
                {
                    switch (quest.goal.type)
                    {
                        case QuestGoal.GoalType.Gather:
                            GatheredItem(quest);
                            break;
                        case QuestGoal.GoalType.Travel:
                            //- Called by QuestTrigger
                            break;
                        case QuestGoal.GoalType.Talk:
                            //- Called by DialogManager
                            break;
                        case QuestGoal.GoalType.Stat:
                            CheckStats(quest);
                            break;
                        case QuestGoal.GoalType.Use:
                            //-
                            break;
                        case QuestGoal.GoalType.Bring:
                            BringBack(quest);
                            break;
                        default:
                            break;
                    }
                }
            }
        }
    }

    #region Private Quest Logic
    private bool IsItemGathered(int itemID, int amount)
    {
        InventoryManager im = InventoryManager.GetInstance();

        foreach (InventoryItem item in im.inventory)
        {
            if (item.itemData.itemId == itemID)
            {
                if (item.quantity >= amount)
                {
                    return true;
                }
            }
        }
        return false;
    }

    private void GatheredItem(QuestData quest)
    {
        if (IsItemGathered(quest.goal.itemID, quest.goal.requiredAmount))
        {
            quest.isFinished = true;
        }
    }
    

    private void CheckStats(QuestData quest)
    {
        int[] stats = { ps.charisma, ps.sexappeal, ps.intelligence, ps.strength, ps.gold };

        bool temp = true;

        for (int i = 0; i < 5; i++)
        {
            if (stats[i] <= quest.goal.requiredStat[i])
            {
                temp = false;
            }
        }

        quest.isFinished = temp;
    }

    private void ItemUsed(QuestData quest)
    {
        //Items cannot be used
    }

    private void BringBack(QuestData quest)
    {
        if (IsItemGathered(quest.goal.itemID, quest.goal.requiredAmount))
        {
            GameObject[] persons = GameObject.FindGameObjectsWithTag("Person");

            foreach (var p in persons)
            {
                DialogData d = p.GetComponent<DialogSystem>().GetComponent<DialogData>();
                if (d.dialogId == quest.goal.dialogID.ToString())
                {
                    quest.isFinished = true;
                }
            }
        }
    }

    #endregion
    //-----------------------------------------------
    #region Public Quest Logic

    public void DialogStarted(int dialogID)
    {
        foreach (QuestData q in questsArray)
        {
            if (q.goal.dialogID == dialogID)
            {
                q.isFinished = true;
            }
        }
    }
    #endregion

    #region GameSate
    public int[] GetActiveQuests()
    {
        List<int> iList = new List<int>();

        foreach (QuestData q in questsArray)
        {
            if (q.isActive)
            {
                iList.Add(Convert.ToInt32(q.questID));
            }
        }
        return iList.ToArray();
    }

    public int[] GetFinishedQuests()
    {
        List<int> iList = new List<int>();

        foreach (QuestData q in questsArray)
        {
            if (q.isFinished)
            {
                iList.Add(Convert.ToInt32(q.questID));
            }
        }
        return iList.ToArray();
    }

    public void LoadActiveQuests(int[] array)
    {
        foreach (QuestData q in questsArray)
        {
            for (int i = 0; i < array.Length; i++)
            {
                if (Convert.ToInt32(q.questID) == array[i])
                {
                    q.isActive = true;
                }
            }
        }
    }

    public void LoadFinishedQuests(int[] array)
    {
        foreach (QuestData q in questsArray)
        {
            for (int i = 0; i < array.Length; i++)
            {
                if (Convert.ToInt32(q.questID) == array[i])
                {
                    q.isFinished = true;
                }
            }
        }
    }
    #endregion

    #endregion
}
