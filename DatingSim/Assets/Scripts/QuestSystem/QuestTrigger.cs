﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestTrigger : MonoBehaviour
{
    public QuestData questData;
    
    [Tooltip("True: startet eine Quest; False: beendet eine Quest")]
    public bool questStart;

    public void TriggerQuest()
    {
        if (!questStart)
        {
            questData.isFinished = true;
        }
        else
        {
            questData.isActive = true;
        }
        

        // ---

        //QuestManager.GetInstance().PositionReached(questData);
    }
}
