﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class QuestGoal
{
    public GoalType type;

    /// <summary>
    /// Item required for Use-Quests or Gather-Quests.
    /// </summary>
    public int itemID;
    public int requiredAmount;

    /// <summary>
    /// Position of the NPC or the Objective the player needs to go to.
    /// </summary>
    public Collider positionCollider;
    public bool positionReached = false;

    /// <summary>
    /// Value of stats needed.
    /// </summary>
    public int[] requiredStat = new int[5];

    /// <summary>
    /// ID of the dialog that needs to be started.
    /// </summary>
    public int dialogID;

    public enum GoalType
    {
        Gather, //
        Travel, //
        Talk, //
        Stat, //
        Use, //
        Bring //
    }
}
