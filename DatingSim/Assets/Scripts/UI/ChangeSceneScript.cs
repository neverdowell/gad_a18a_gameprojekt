﻿using UnityEngine;
using UnityEngine.SceneManagement;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class ChangeSceneScript : MonoBehaviour {

    public GameObject loadingPanel;

    private bool isLoading = false;

    #region helper
    public void ChangeScene(string scene){
        if (!isLoading) { 
            if (loadingPanel != null){
                loadingPanel.SetActive(true);
            }
            SceneManager.LoadSceneAsync(scene);
            isLoading = true;
        }
    }

    public void Exit() {
#if UNITY_STANDALONE
        Application.Quit();
#elif UNITY_EDITOR
    Editor.isPlaying = false;
#endif
    }
    #endregion
}
