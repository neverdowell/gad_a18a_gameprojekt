﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeSceneAfterTime : MonoBehaviour {

    [Header("Skip scene settings")]
    [Tooltip("Name of scene to load after skipping")]
    public string nextSceneName = "";
    [Tooltip("If true, scene can be skipped by pressing keys.")]
    public bool changeSceneByKeyActive = true;
    [Tooltip("If true, scene is skipped automatically after given time.")]
    public bool changeSceneByTimeActive = true;

    [Header("Change scene by keydown")]
    [Tooltip("If true, any key down skips scene, else only skipKeyCode is checked.")]
    public bool skipOnAnyKey = true;
    [Tooltip("Key code for skipping scene.")]
    public KeyCode skipKeyCode = KeyCode.Space;

    [Header("Change scene by time")]
    [Tooltip("Duration until scene is skipped automatically")]
    public float autoSkippingTimer = 5.0f;


    private bool isChangingScene = false;


    #region unity livecycle
    void Update () {
        if (!isChangingScene) {
            // change scene by input
            if (changeSceneByKeyActive) {
                if ((Input.anyKeyDown && skipOnAnyKey) || Input.GetKeyDown(skipKeyCode)) {
                    ChangeScene(nextSceneName);
                }
            }

            // change scene by timer
            if (changeSceneByTimeActive) {
                autoSkippingTimer -= Time.deltaTime;
                if (autoSkippingTimer < 0) {
                    ChangeScene(nextSceneName);
                }
            }
        }
    }
    #endregion




    #region helper
    public void ChangeScene(string scene)
    {
        SceneManager.LoadScene(scene);
        isChangingScene = true;
    }
    #endregion
}
