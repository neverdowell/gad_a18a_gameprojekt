﻿using UnityEngine;

public class GameUIScript : MonoBehaviour {

    [Header("Settings")]
    public KeyCode openIngameMenuKeyCode = KeyCode.Escape;
    [Header("Panels")]
    public GameObject CharacterStatsPanel;
    public GameObject DialogPanel;
    public GameObject InventoryPanel;
    public GameObject MessagePanel;
    public GameObject PlayerStatsPanel;
    public GameObject QuestPanel;
    public GameObject IngameMenuPanel;
    [Header("DISABLE TEST")]
    public bool showTestUI;
    public GameObject TestPanel;




    void Start() {
        if (!showTestUI) {
            TestPanel.SetActive(false);
        }
    }

    void Update() {
        if (Input.GetKeyDown(openIngameMenuKeyCode)) {
            ToggleIngameMenu();
        }
    }



    #region helper
    public void ToggleStatsPanels(){
        CharacterStatsPanel.SetActive(!CharacterStatsPanel.activeSelf);
        PlayerStatsPanel.SetActive(!PlayerStatsPanel.activeSelf);
    }

    public void ToggleInventoryPanel(){
        
        InventoryPanel.SetActive(!InventoryPanel.activeSelf);
    }

    public void ToggleQuestPanel(){
        QuestPanel.SetActive(!QuestPanel.activeSelf);
    }

    public void ToggleIngameMenu() {
        IngameMenuPanel.SetActive(!IngameMenuPanel.activeSelf);
    }




    public void SetCharacterStatsVisibility(bool isVisible) {
        CharacterStatsPanel.SetActive(isVisible);
    }

    public void SetDialogVisibility( bool isVisible ) {
        DialogPanel.SetActive(isVisible);
    }

    public void SetInventoryVisibility( bool isVisible ) {
        InventoryPanel.SetActive(isVisible);
    }

    public void SetMessageVisibility( bool isVisible ) {
        MessagePanel.SetActive(isVisible);
    }

    public void SetQuestVisibility( bool isVisible ) {
        QuestPanel.SetActive(isVisible);
    }

    public void SetPlayerStatsVisibility( bool isVisible ) {
        PlayerStatsPanel.SetActive(isVisible);
    }
    #endregion
}
