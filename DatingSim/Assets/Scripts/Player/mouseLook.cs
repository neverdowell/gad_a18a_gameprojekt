﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mouseLook : MonoBehaviour
{
    public float mouseSensetivity = 100f;
    public Transform playerBody;
    float xRotation = 0f;

    //Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        float mouseX = Input.GetAxis("Mouse X") * mouseSensetivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensetivity * Time.deltaTime;

        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);

        transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
        playerBody.Rotate(Vector3.up * mouseX);
    }
<<<<<<< HEAD
    /*
    /// <summary>
    /// Instantiates the script's instance
    /// </summary>
    private void InstantiateSingleton()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        } else
        {
            if (instance != this)
            {
                Destroy(this.gameObject);
            }
        }
    }

    /// <returns>Returns the script's instance</returns>
    public static SQLite GetInstance()
    {
        return instance;
    }

    /// <summary>
    /// Gets called when the script instance loads
    /// </summary>
    private void Awake()
    {
        InstantiateSingleton();
    }
    */
}
