﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movePlayer : MonoBehaviour
{
    public CharacterController controller;
    public Transform cam;

    public float speed = 6f;
    public float sprintSpeed = 10f;

    public float turnSmoothTime = 0.1f;
    float turnSmoothVelocity;

    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;
    bool isGrounded;

    private Vector3 velocity;
    public float gravity = -9.81f;
    public float jumpHeight = 3f;

    public KeyCode sprintTaste;

    // Update is called once per frame
    void Update()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if(isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }

        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);

        float x = Input.GetAxisRaw("Horizontal");
        float z = Input.GetAxisRaw("Vertical");

        Vector3 direction = new Vector3(x, 0, z);

        float targetAngle = Mathf.Atan2(direction.x, direction.y) * Mathf.Rad2Deg + cam.eulerAngles.y;
        float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);

        transform.rotation = Quaternion.Euler(0f, angle, 0f);

        if(direction.magnitude >= 0.1f)
        {
            Vector3 moveDir = new Vector3(0, 0, 0);
            if (z != -1 || x != 0)
            {
                moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
            }
            else
            {
                moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.back;
            }

            if (Input.GetKey(sprintTaste) && z > 0)
            {
                controller.Move(moveDir * sprintSpeed * Time.deltaTime);
            }
            else
            {
                controller.Move(moveDir * speed * Time.deltaTime);
            }
            
        }


        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
        }
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        switch (hit.gameObject.tag)
        {
            case "Item":
                SelectItem(hit.gameObject.GetComponent<ItemScript>());
                Destroy(hit.gameObject);
                break;
            case "Dialog":
                Debug.Log("Dialog Triggered");
                //DialogTrigger dialogTrigger = hit.gameObject.GetComponent<DialogTrigger>();
                //DialogSystem dialogSystem;
                //if (dialogTrigger != null) {
                //    dialogSystem.TryStartDialog(dialogTrigger.dialogDatas[0]);
                //}
                break;
        }
    }

    private void SelectItem(ItemScript itemScript){
        //Item aufheben
        Debug.Log("Item aufheben");
        InventoryManager.GetInstance().AddToInventory(itemScript.itemData, itemScript.quantity);
    }
   
}
