﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;

[System.Serializable]
public class GameState
{

    public int[] activeQuests;
    public int[] finishedQuests;

    public GameState() { }

}
