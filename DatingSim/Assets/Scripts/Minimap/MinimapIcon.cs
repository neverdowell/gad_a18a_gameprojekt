﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinimapIcon : MonoBehaviour
{

    public GameObject minimapIcon;

    // Start is called before the first frame update
    void Start()
    {
        Instantiate(minimapIcon, this.transform);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
