﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageManager : MonoBehaviour
{
    public GameObject messagePanel;
    public Text messageText;
    public float messageDuration = 2.0f;
    public float newMessageHintDuration = 0.1f;
    public Color messageColor = Color.grey;
    public Color newMessageHintColor = Color.white;




    private Queue<string> messages;
    private string currentMessage;
    private float currentMessageDuration;




    void Start(){
        messages = new Queue<string>();
        currentMessageDuration = -1;
    }

    void Update(){
        currentMessageDuration -= Time.deltaTime;
        if (currentMessageDuration <= 0) {
            if (currentMessage != null) {
                messagePanel.SetActive(false);
                currentMessage = null;
            }
            TryShowNextMessage();
        }
    }

    public void AddMessage(string message) {
        messages.Enqueue(message);
    }

    private void TryShowNextMessage() {
        if (messages.Count > 0) {
            currentMessage = messages.Dequeue();
            messageText.text = currentMessage;
            messagePanel.SetActive(true);
            currentMessageDuration = messageDuration;
            messageText.text = currentMessage;
            messageText.color = newMessageHintColor;
            Invoke("SwitchColor", newMessageHintDuration);
        }
    }

    public void SwitchColor() {
        messageText.color = messageColor;
    }
}
