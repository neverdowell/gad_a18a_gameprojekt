﻿using UnityEngine;

[CreateAssetMenu(menuName = "DatingSim/Create new CharacterData")]
public class CharacterData : ScriptableObject
{
    [Header("Character Data")]
    public int characterId;
    public string name;
    public GameObject prefab;
    public Sprite sprite;
}