﻿using UnityEngine;
using UnityEngine.UI;

public class InventoryItemUIScript : MonoBehaviour {

    public ItemData itemData;
    public Image inventoryItemImage;
    public Text inventoryItemName;
    public Text quantity;



    #region helper
    public void SelectItem() {
        InventoryManager.GetInstance().SelectItem(itemData);
    }
    #endregion
}
