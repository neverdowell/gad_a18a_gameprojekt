﻿using System.Collections.Generic;
using UnityEngine;

public class InventoryUIScript : MonoBehaviour
{

    public GameObject contentGameObject;
    public GameObject inventoryItemPrefab;
    public GameObject emptyInventoryItemPrefab;



    private InventoryManager inventoryManager;




    #region lifecycle
    private void Start() {
        inventoryManager = InventoryManager.GetInstance();
        RefreshInventory();
    }

    private void OnDestroy() {
        inventoryManager.inventoryUIScript = null;
    }
    #endregion




    #region helper
    public void RefreshInventory() {
        // clear inventory items
        foreach (Transform child in contentGameObject.transform){
            Destroy(child.gameObject);
        }
        if (inventoryManager.inventory.Count == 0){ // add empty item
            Instantiate(emptyInventoryItemPrefab, contentGameObject.transform);
        } else {
            // add current items
            foreach (InventoryItem inventoryItem in inventoryManager.inventory) {
                ItemData itemData = inventoryItem.itemData;
                GameObject newInventoryItemPrefab = Instantiate(inventoryItemPrefab, contentGameObject.transform);
                newInventoryItemPrefab.name = itemData.name;
                InventoryItemUIScript inventoryItemUIScript = newInventoryItemPrefab.GetComponent<InventoryItemUIScript>();
                inventoryItemUIScript.itemData = itemData;
                inventoryItemUIScript.inventoryItemImage.sprite = itemData.sprite;
                inventoryItemUIScript.inventoryItemName.text = itemData.name;
                inventoryItemUIScript.quantity.text = inventoryItem.quantity.ToString();
            }
        }
    }
    #endregion
}
