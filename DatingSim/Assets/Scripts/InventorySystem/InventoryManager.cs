﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryManager : MonoBehaviour {
    public List<InventoryItem> inventory = new List<InventoryItem>();
    //public List<ItemData> inventory = new List<ItemData>();
    public ItemData selectedItemData;
    public Image selectedItemImage;

    public InventoryUIScript inventoryUIScript;


    private GameManager gameManager;


    #region singleton
    private static InventoryManager instance;
    private InventoryManager() { }

    public static InventoryManager GetInstance() {
        return instance;
    }
    #endregion




    #region lifecycle
    private void Awake() {
        if (instance == null) {
            instance = this;
        } else {
            if (instance != this) {
                Destroy(this.gameObject);
            }
        }
    }

    private void Start() {
        gameManager = GameManager.GetInstance();
    }
    #endregion




    #region Inventory Feature
    public InventoryItem GetInventoryItem( ItemData itemData ) {
        foreach (InventoryItem inventoryItem in inventory) {
            if (inventoryItem.itemData.Equals(itemData)) {
                return inventoryItem;
            }
        }
        return null;
    }

    public void AddToInventory(ItemData itemData, int quantity) {
        InventoryItem inventoryItem = GetInventoryItem(itemData);
        if (inventoryItem == null) {
            inventory.Add(new InventoryItem(itemData, quantity));
            Debug.Log(quantity + " " + itemData.name + "(s) aufgehoben");
            //gameManager.messageManger.AddMessage(quantity + " " + itemData.name + " aufgehoben");
        } else {
            inventoryItem.quantity += quantity;
            Debug.Log("Zusätzliche " + quantity + " " + itemData.name + "(s) aufgehoben");
        }
        if (inventoryUIScript != null) {
            inventoryUIScript.RefreshInventory();
        }
        
    }

    public bool ContainsInInventory(ItemData itemData ) {
        return GetInventoryItem(itemData) != null;
    }



    public void RemoveFromInventory(ItemData itemData, int quantity ) {
        InventoryItem inventoryItem = GetInventoryItem(itemData);
        if (inventoryItem != null) {
            if (inventoryItem.quantity > quantity) {
                inventoryItem.quantity -= quantity;
            } else if (inventoryItem.quantity == quantity) {
                inventory.Remove(inventoryItem);
            } else {
                Debug.Log("RemoveFromInventory: Inventory enthält nicht genügens Items:" + itemData + ": " + inventoryItem.quantity + " statt " + quantity);
            }
            if(inventoryUIScript != null) {
                inventoryUIScript.RefreshInventory();
            }
        } else {
            Debug.Log("RemoveFromInventory: Inventory enthält diese Items nicht: " + itemData.name);
        }
    }

    public void SelectItem( ItemData selectedItemData ) {
        this.selectedItemData = selectedItemData;
        if (selectedItemData != null) {
            selectedItemImage.enabled = true;
            selectedItemImage.sprite = selectedItemData.sprite;
        } else {
            selectedItemImage.enabled = false;
        }
        ShowInventory(false);
    }

    public void ShowInventory( bool show ) {
        inventoryUIScript.RefreshInventory();
        inventoryUIScript.gameObject.SetActive(show);
    }
    #endregion
}
