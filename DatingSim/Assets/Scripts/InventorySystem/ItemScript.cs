﻿using UnityEngine;

public class ItemScript : MonoBehaviour
{
    public int quantity;
    public ItemData itemData;
    public bool destroy = true;
}
