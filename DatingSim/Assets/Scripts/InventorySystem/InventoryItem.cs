﻿using UnityEngine;
using System.Collections;

public class InventoryItem {

    public int quantity;
    public ItemData itemData;

    public InventoryItem(ItemData itemData, int quantity ) {
        this.quantity = quantity;
        this.itemData = itemData;
    }
}
