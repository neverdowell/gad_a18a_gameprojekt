﻿using UnityEngine;

public class InventoryTestScript : MonoBehaviour
{
    public ItemData[] itemDatas;



    private InventoryManager inventoryManager;



    private void Start() {
        inventoryManager = InventoryManager.GetInstance();
    }

    #region button callbacks
    public void AddItem1() {
        if (itemDatas.Length > 0) {
            inventoryManager.AddToInventory(itemDatas[0], 1);
        } else {
            Debug.Log("AddItem1: No ItemData objects in itemDatas-Array");
        } 
    }

    public void RemoveItem1() {
        if (itemDatas.Length > 0) {
            inventoryManager.RemoveFromInventory(itemDatas[0], 1);
        } else {
            Debug.Log("RemoveItem1: No ItemData objects in itemDatas-Array");
        }
    }

    public void AddRandomItem() {
        if (itemDatas.Length > 0) {
            int index = (int)(Random.value * itemDatas.Length);
            inventoryManager.AddToInventory(itemDatas[index], 1);
        } else {
            Debug.Log("AddRandomItem: No ItemData objects in itemDatas-Array");
        }
    }
    #endregion
}
